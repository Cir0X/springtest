package net.cr0wd;

import net.cr0wd.springtest.SpringtestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringtestApplication.class)
@WebAppConfiguration
public class SpringtestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
