package net.cr0wd.springtest.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column(unique = true)
    @Size(min = 3, max = 20)
    private String username;

    private int age;

    private double weight;

    @ManyToOne(cascade = CascadeType.ALL)
    private Address address;

    public User() {
    }

    public User(String username, int age, double weight, Address address) {
        this.username = username;
        this.age = age;
        this.weight = weight;
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
