package net.cr0wd.springtest.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long>{

    Optional<Address> findByZipcode(String username);

}
