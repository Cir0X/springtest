package net.cr0wd.springtest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;

@Entity
public class Address {

    @Id
    @GeneratedValue
    private Long id;

    @Max(99999)
    private Integer zipcode;

    private String street;

    public Address() {
    }

    public Address(int zipcode, String street) {
        this.zipcode = zipcode;
        this.street = street;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
