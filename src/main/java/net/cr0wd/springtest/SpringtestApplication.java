package net.cr0wd.springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class SpringtestApplication {

//    @Bean
//    CommandLineRunner init(UserRepository userRepository, AddressRepository addressRepository) {
//
//        return (evt) -> Collections.singletonList(
//                "vader"
//        ).forEach(
//                a -> {
//                    Address address = new Address(53333, "Foo Street 13");
//                    User user = new User(a, 40, 85.0, address);
//                    userRepository.save(user);
//                }
//        );
//
//    }

    public static void main(String[] args) {
        SpringApplication.run(SpringtestApplication.class, args);
    }
}
