package net.cr0wd.springtest.controller;

import net.cr0wd.springtest.model.User;
import net.cr0wd.springtest.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    User createUser(@RequestBody @Valid User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new InvalidUserException();
        }

//        validateUser(user); // for more specific validations

        return userRepository.save(user);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    List<User> readUsers() {
        return userRepository.findAll();
    }

    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
    User readUser(@PathVariable String username) {

        Optional<User> user = userRepository.findByUsername(username);

        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException(username);
        }

    }

    private void validateUser(User user) {

        if (user.getUsername().isEmpty()) {
            throw new UsernameIsEmptyException();
        }

        if (userRepository.findByUsername(user.getUsername()).isPresent()) {
            throw new UsernameAlreadyExistsException(user.getUsername());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private class InvalidUserException extends RuntimeException {
        InvalidUserException() {
            super("user is invalid");
        }
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    private class UserNotFoundException extends RuntimeException {

        UserNotFoundException(String username) {
            super("could not find user " + username);
        }

    }

    @ResponseStatus(HttpStatus.CONFLICT)
    private class UsernameAlreadyExistsException extends RuntimeException {

        UsernameAlreadyExistsException(String username) {
            super("username " + username + " already exists");
        }

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private class UsernameIsEmptyException extends RuntimeException {

        UsernameIsEmptyException() {
            super("username is empty");
        }

    }
}
